/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100212
Source Host           : localhost:3306
Source Database       : blog_app

Target Server Type    : MYSQL
Target Server Version : 100212
File Encoding         : 65001

Date: 2019-02-14 23:18:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for blogs
-- ----------------------------
DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blogs_slug_name_unique` (`slug_name`),
  UNIQUE KEY `blogs_user_id_unique` (`user_id`),
  CONSTRAINT `blogs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of blogs
-- ----------------------------
INSERT INTO `blogs` VALUES ('1', '1', 'food', 'food', null, null);
INSERT INTO `blogs` VALUES ('2', '2', 'travel', 'travel', null, null);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_id` int(10) unsigned NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_name` (`slug_name`) USING BTREE,
  KEY `categories_blog_id_foreign` (`blog_id`),
  CONSTRAINT `categories_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', '1', 'sweets', 'sweets', null, null);
INSERT INTO `categories` VALUES ('2', '1', 'meat', 'meat', null, null);
INSERT INTO `categories` VALUES ('3', '1', 'dairy', 'dairy', null, null);

-- ----------------------------
-- Table structure for category_posts
-- ----------------------------
DROP TABLE IF EXISTS `category_posts`;
CREATE TABLE `category_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_posts_post_id_foreign` (`post_id`),
  KEY `category_posts_category_id_foreign` (`category_id`),
  CONSTRAINT `category_posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `category_posts_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of category_posts
-- ----------------------------
INSERT INTO `category_posts` VALUES ('1', '1', '1');
INSERT INTO `category_posts` VALUES ('2', '2', '1');
INSERT INTO `category_posts` VALUES ('3', '2', '2');
INSERT INTO `category_posts` VALUES ('5', '3', '3');
INSERT INTO `category_posts` VALUES ('6', '2', '3');
INSERT INTO `category_posts` VALUES ('7', '3', '2');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('1', 'test', 'test', 'Your first management position will probably come with some growing pains. Sure, it’s exciting to land a larger office, new title, and pay bump, but all those things come with new expectations. You’re held to a different standard now: In addition to doing your job, you’re also supposed to be a leader, coach, and mentor to your subordinates — and you probably shouldn’t be letting off steam with them in the break room or participating in office gossip sessions.', '2019-06-22 07:18:03', '2019-06-22 07:18:03');
INSERT INTO `posts` VALUES ('2', 'test gg', 'test-gg', 'The transition can be especially tricky if your employees don’t respect you, if there’s any kind of personality clash, or if you don’t have adequate support. It can feel overwhelming to navigate. It can be lonely.\r\n\r\nBelow, people who have been there talk about what they wish they’d known before stepping into a managerial role, the challenges they faced in their new positions, and how they coped.', '2019-06-22 07:19:37', '2019-06-22 07:19:37');
INSERT INTO `posts` VALUES ('3', 'qq', 'qq', 'I thought that because of my new title, my employees would automatically respect my authority. That didn’t work very well. Some resented my promotion; others just didn’t want to fall in line. I told my manager, “You’ve got to have a meeting and tell everyone I’m the new sales manager,” but he said telling them wouldn’t do it — I needed to earn the title, and only time and hard work would make that happen. Ultimately, he was right. Working with the salespeople in the trenches day in and day out is what really elevated my status. I wish I knew that having a title doesn’t make you a manager — being an effective leader is what gets people to fall in behind you.', null, null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'tasos', 'tasos@in.gr', '$2y$10$lK4nrNpX0MAFeFQo4Y1kYuhx4pOP3H5BUa28COIf1HDEjPTD7kyMi', null, null);
INSERT INTO `users` VALUES ('2', 'nikos', 'nikos@in.gr', '123a', null, null);
