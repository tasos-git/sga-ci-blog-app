<?php

class Post_model extends CI_Model {

    private $id;
    public $title;
    public $content;
    public $slug_name;
    public $created_at;
    public $updated_at;
    
    public function __construct()
    {
        $this->load->database();
    }
    

    public function get_post($slug = FALSE)
    {
        $query = $this->db->get_where('posts', array('slug_name' => $slug));
        return $query->row();
    }


    public function get_posts()
    {
        $this->db->order_by('id','DESC');
        $query = $this->db->get('posts');

        return $query->result();
    }


    public function get_post_categories($post_id)
    {
        $this->db->select('categories.*')
            ->from('categories')
            ->join('category_posts', 'categories.id = category_posts.category_id')
            ->where('category_posts.post_id =', $post_id);
        $query = $this->db->get();
        
        foreach ($query->result() as $category)
        {
            $categories[] = $category;
        }

        return $categories;
    }


    public function get_latest_blog_posts($blog_slug_name)
    {
        $this->db->select('posts.*, blogs.title as blog_title, blogs.slug_name as blog_slug_name')
            ->from('posts')
            ->join('category_posts', 'posts.id = category_posts.post_id')
            ->join('categories', 'categories.id = category_posts.category_id')
            ->join('blogs', 'blogs.id = categories.blog_id')
            ->where('blogs.slug_name =', $blog_slug_name)
            ->group_by("posts.id")
            ->order_by('posts.updated_at', 'DESC')
            ->limit(10);
        $query = $this->db->get();
        $posts = [];

        foreach ($query->result() as $post)
        {
            $posts[] = $post;
        }
        
        return $posts;
    }


    public function create_post()
    {
        $slug = url_title($this->input->post('title'));
        $data = array(
            'title' => $this->input->post('title'),
            'slug_name' => $slug,
            'content' => $this->input->post('content')
        );
        return $this->db->insert('posts', $data);
    }


    public function delete_post($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('posts');
        return true;
    }
}
