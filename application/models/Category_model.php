<?php

class Category_model extends CI_Model {

	private $id;
	private $blog_id;
    public $title;
    public $slug_name;
    public $created_at;
    public $updated_at;
    
    public function __construct()
    {
        $this->load->database();
    }


    public function get_category($slug_name)
    {
        $query = $this->db->get_where('categories', array('slug_name' => $slug_name));
        return $query->row();
    }
    

    public function get_posts($slug_name)
    {
        $this->db->select('posts.*')
            ->from('posts')
            ->join('category_posts', 'posts.id = category_posts.post_id')
            ->join('categories', 'categories.id = category_posts.category_id')
            ->where('categories.slug_name =', $slug_name);
        $query = $this->db->get();
        $posts = [];
        
        foreach ($query->result() as $post)
        {
            $posts[] = $post;
        }

        return $posts;
    }

}