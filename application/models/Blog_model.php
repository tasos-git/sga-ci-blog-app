<?php

class Blog_model extends CI_Model {

    private $id;
    public $title;
    public $slug_name;
    public $created_at;
    public $updated_at;


    public function __construct()
    {
        $this->load->database();
    }


    public function get_blog($slug_name)
    {
        $query = $this->db->get_where('blogs', array('slug_name' => $slug_name));
        return $query->row();
    }


    public function get_blogs()
    {
        $this->db->order_by('title', 'ASC');
        $query = $this->db->get('blogs');
        return $query->result();
    }

}
