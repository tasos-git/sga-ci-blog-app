<?php

class User_model extends CI_Model {

    private $id;
    private $password;
    public $name;
    public $email;
    public $created_at;


    public function __construct()
    {
        
    }


    public function get_user($id)
    {
        $query = $this->db->get_where('users', array('id' => $id));
        return $query->row();
    }


    public function get_user_by_email($email)
    {
        $query = $this->db->get_where('users', array('email' => $email));
        return $query->row();
    }


    public function logged_in() {
        return isset($_SESSION['admin']);
    }


    public function logout() {
        session_destroy();
    }
    

    public function verify($email, $password)
    {
        $query = $this->db->query('SELECT * FROM users WHERE email = ? LIMIT 1', [$email]);
        
        if($query->num_rows()) {
            $user = $query->row();

            if (password_verify($password, $user->password)) {
                $_SESSION['admin'] = [$user->name, $user->email];
                return true;
            }
        }
        
        return false;
        
    }


}