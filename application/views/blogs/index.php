<div class="card-group">
<?php foreach ($blogs as $blog) : ?>
	<div class="card">
		<div class="card-body text-center">
			<h2 class="display-3 card-title mb-4"><?php echo $blog->title; ?></h2>
			<a href="<?php echo site_url('/blogs/'.$blog->slug_name); ?>" class="btn btn-link">View Posts</a>
		</div>
	</div>
<?php endforeach; ?>
</div>
