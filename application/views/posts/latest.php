<h1> <?= $title ?> </h1>
<?php foreach ($posts as $post) : ?>
	<div class="p-3">
		<h3><a style="font-size: 2rem;" class="btn btn-link" href="<?php echo site_url('/posts/'.$post->slug_name); ?>"><?php echo $post->title; ?></a></h3>
		<div class="text-right mb-2"><small class="post-date">Posted on: <?php echo $post->created_at; ?></small></div>
		<p>
			<?php echo substr($post->content,0, 100); ?>...
		</p>
	</div>
	<hr>
<?php endforeach; ?>
<p class="text-center my-2">
	<a href="<?php echo site_url('/blogs/'.$blog_slug_name); ?>/posts">View All Posts</a>
</p>
