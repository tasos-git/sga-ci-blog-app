<div class="text-right"><small class="post-date">Posted on: <?php echo $post->created_at; ?></small></div>
<h2 class="display-3 text-center mb-4"> <?php echo $post->title; ?></h2>

<div class="post-body" style="line-height: 1.6;font-size: 1.125rem;width:65%;margin: 0 auto;text-align:justify;">

	<?php echo $post->content; ?>
	<hr>
	<p class="text-right">
		<span>Categories: </span>
	<?php foreach ($categories as $category) : ?>
		<a href="<?php echo site_url('/categories/'.$category->slug_name); ?>" class="px-2"><?php echo $category->title; ?></a>
	<?php endforeach; ?>
	</p>
</div>