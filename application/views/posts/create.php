@extends('layouts.app')

@section('content')
<div class="row align-items-center justify-content-around">
    <div class="col col-md-5 align-self-start">
        <h1 class="display-4 text-left">Create new Film</h1>
        <a href="{{ route('film.index') }}"><- Back to Films</a>
		@if ($errors->any())
		<hr>
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				    <li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
    </div>
    <div class="col col-md-6 align-self-center">
		<form class="jumbotron" method="POST" action="{{ route('film.store') }}" style="padding: 1rem 2rem;">
			@csrf
			<div class="form-group">
				<label>Name</label>
				<input type="text" class="form-control" id="_name" value="{!! old('_name') !!}" name="_name">
			</div>
			<div class="form-group">
				<label>Rating</label>
				<select class="form-control" id="_rating" name="_rating">
					<option></option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
				</select>
			</div>
			<div class="form-group">
				<label>Price</label>
				<input type="number" step="0.01" min=0 class="form-control" value="{!! old('_price') !!}" id="_price" name="_price">
			</div>
			<div class="form-group">
				<label>Country</label>
				<select class="form-control film_country" name="_country"></select>
			</div>
			<div class="form-group">
				<label>Genre</label>
				<select multiple class="form-control film_genre" name="_genre[]"></select>
			</div>
			<div class="form-group">
				<label>Release Date</label>
				<input type="date" class="form-control" id="_release_date" value="{!! old('_release_date') !!}" name="_release_date">
			</div>
			<div class="form-group">
				<label>Photo</label>
				<input type="text" class="form-control" id="_photo" value="{!! old('_photo') !!}" name="_photo">
			</div>
			<hr>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
    </div>
</div>
@endsection

@section('script_inline')
<script>
    $(function () {
        $(document).ready(function() {
        	var genre_url = '{{ route('genre.index') }}';
			$('.film_genre').select2({
				ajax: {
					url: genre_url,
					processResults: function (response) {
						return {
							results: response
						};
					},
					cache: true
				}
			});

			var country_url = '{{ route('country.index') }}';
			$('.film_country').select2({
				ajax: {
					url: country_url,
					processResults: function (response) {
						return {
							results: response
						};
					},
					cache: true
				}
			});
        });
    });        
</script>
@endsection