<div class="py-4" style="width:60%;margin:0 auto;">
    <form method="POST" action="<?php echo base_url(); ?>auth/login">
        <div class="form-group">
            <label for="email">Email address</label>
            <input class="form-control mr-sm-2" type="email" name="email" placeholder="Email" aria-label="Email">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input class="form-control mr-sm-2" type="password" name="password" placeholder="Password" aria-label="Password">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>