<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {


	public function index()
	{
		
	}


	public function view($slug_name = NULL)
	{
		
	}


	public function view_posts($slug_name = NULL)
	{
		$data['posts'] = $this->Category_model->get_posts($slug_name);
		if(empty($data['posts'])){
			show_404();
		}
		
		$category = $this->Category_model->get_category($slug_name);

		$data['title'] = 'Category: ' . $category->title;
		
		$this->load->view('layout/header');
		$this->load->view('posts/category', $data);
		$this->load->view('layout/footer');
	}

}
