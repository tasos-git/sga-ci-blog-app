<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {


	public function index()
	{
		
		$data['title'] = 'All Posts';
		$data['posts'] = $this->Post_model->get_posts();
		
		$this->load->view('layout/header');
		$this->load->view('posts/index',$data);
		$this->load->view('layout/footer');
	}


	public function view($slug = NULL)
	{
		$data['post'] = $this->Post_model->get_post($slug);
		
		if(empty($data['post'])){
			show_404();
		}

		$data['categories'] = $this->Post_model->get_post_categories($data['post']->id);
		$data['title'] = $data['post']->title;

		$this->load->view('layout/header');
		$this->load->view('posts/view',$data);
		$this->load->view('layout/footer');
	}


	public function view_latest($slug_name = NULL)
	{
		$data['posts'] = $this->Post_model->get_latest_blog_posts($slug_name);
		if(empty($data['posts'])){
			show_404();
		}

		$blog = $this->Blog_model->get_blog($slug_name);

		$data['title'] = $blog->title . ': Latest Posts';
		$data['blog_slug_name'] = $blog->slug_name;
		
		$this->load->view('layout/header');
		$this->load->view('posts/latest', $data);
		$this->load->view('layout/footer');
	}


	public function create()
	{
		$data['title'] = 'Create Post';
		//	$data['categories'] = $this->post_model->get_categories();
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('body', 'Body', 'required');
		if($this->form_validation->run() === FALSE){
		$this->load->view('layout/header');
		$this->load->view('posts/create', $data);
		$this->load->view('layout/footer');
		} else {
		
		$this->Post_model->create_post();
		redirect('posts');
		}
	}


	public function delete($id)
	{
		$this->Post_model->delete_post($id);
		redirect('posts');
	}


	public function edit($slug)
	{
		$data['post'] = $this->post_model->get_posts($slug);
		
		if(empty($data['post'])){
			show_404();
		}
		
		$data['title'] = 'Edit Post';
		
		$this->load->view('layout/header');
		$this->load->view('posts/edit', $data);
		$this->load->view('layout/footer');
	}
}
