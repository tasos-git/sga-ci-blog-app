<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends CI_Controller {


	public function index()
	{
		
		//$data['title'] = 'Displaying All Blogs';
		$data['blogs'] = $this->Blog_model->get_blogs();
		
		$this->load->view('layout/header');
		$this->load->view('blogs/index', $data);
		$this->load->view('layout/footer');
	}


	public function view($slug_name = NULL)
	{
		$data['blog'] = $this->Blog_model->get_blog($slug_name);

		if(empty($data['blog'])){
			show_404();
		}

		$data['title'] = $data['blog']['title'];

		$this->load->view('layout/header');
		$this->load->view('blogs/view', $data);
		$this->load->view('layout/footer');
	}
}
