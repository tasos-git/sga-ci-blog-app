<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {


	public function __construct()
    {
    	parent::__construct();
    }


    public function index()
    {
    	if ($this->User_model->logged_in()) {
			redirect('admin', 'refresh');
		}
		
		redirect('auth/login', 'refresh');
    }


    public function view()
    {
    	$this->load->view('layout/header');
		$this->load->view('auth/login');
		$this->load->view('layout/footer');
    }


    public function logout()
    {
    	$this->User_model->logout();
    	redirect('auth/login', 'refresh');
    }


    public function login()
    {
    	if ($this->User_model->verify($this->input->post('email'), $this->input->post('password'))) {
			redirect('admin', 'refresh');
		}
    	
    	redirect('auth/login', 'refresh');
    }


    public function forgot_password()
    {

    }


    public function reset_password($token = NULL)
    {

    }
}
